package Konsolenausgabe;

/**
 * Aufgabe: Recherechieren Sie im Internet !
 * 
 * Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
 *
 * Vergessen Sie nicht den richtigen Datentyp !!
 *
 *
 * @version 1.0 from 21.08.2019
 * @author << Ihr Name >>
 */

public class WeltDerZahlen {

	public static void main(String[] args) {

		/*
		 * *********************************************************
		 * 
		 * Zuerst werden die Variablen mit den Werten festgelegt!
		 *********************************************************** 
		 */
		// Im Internet gefunden ?
		// Die Anzahl der Planeten in unserem Sonnesystem
		char anzahlPlaneten = 8;

		// Anzahl der Sterne in unserer Milchstra�e
		long anzahlSterne = 800000000;

		// Wie viele Einwohner hat Berlin?
		int bewohnerBerlin = 4500000;

		// Wie alt bist du? Wie viele Tage sind das?

		short alterTage = 5840;

		// Wie viel wiegt das schwerste Tier der Welt?
		// Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 200000;

		// Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		int flaecheGroessteLand = 171000000;

		// Wie gro� ist das kleinste Land der Erde?

		double flaecheKleinsteLand = 0.44;

		/*
		 * *********************************************************
		 * 
		 * Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		 *********************************************************** 
		 */
		System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		System.out.println("In der Milchstra�e gibt es: " + anzahlSterne + " Sterne");
		System.out.println("In Berlin wohnen: " + bewohnerBerlin);
		System.out.println("Ich Lebe schon: " + alterTage + " Tage");
		System.out.println("Das schwerste Tier der Welt wiegt in kg : " + gewichtKilogramm);
		System.out.println("Das gr��te Land hat eine Fl�che in km� von: " + flaecheGroessteLand);
		System.out.println("Das kleinste Land hat eine Fl�che in km� von: " + flaecheKleinsteLand);

		System.out.println(" *******  Ende des Programms  ******* ");

	}
}
