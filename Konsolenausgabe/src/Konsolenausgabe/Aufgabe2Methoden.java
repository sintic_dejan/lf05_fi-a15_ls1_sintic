package Konsolenausgabe;

public class Aufgabe2Methoden {

	public static void main(String[] args) {
		System.out.println(vorlage(2.36, 7.87));

	}

	public static double vorlage(double number1, double number2) {
		double x = number1;
		double y = number2;
		return x * y;
	}
}