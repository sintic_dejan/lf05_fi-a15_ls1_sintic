package Konsolenausgabe;

public class Aufgabe4Methoden {

	public static void main(String[] args) {
		
		System.out.println("W�rfel:");
		System.out.println(W�rfel(5));
		
		System.out.println("Quader:");
		System.out.println(Quader(5, 4, 3));
		
		System.out.println("Pyramide:");
		System.out.println(Pyramide(5, 7));
		
		System.out.println("Kugel:");
		System.out.println(Kugel(5));
	}
	
	public static double W�rfel(double a) {
		return a * a * a;
	}
	
	public static double Quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double Pyramide(double a, double h) {
		return a * a * h/3;
	}
	
	public static double Kugel(double r) {
		return 4/3 * Math.pow(r, 3) * Math.PI;
	}
}
	

